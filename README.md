<!--
- Copyright 2020 Han Young <hanyoung@protonmail.com>
- Copyright 2020 Devin Lin <espidev@gmail.com>
- SPDX-License-Identifier: GPL-2.0-or-later
-->

# KWeather
A weather application for Plasma Mobile.

## Backends used
* api.met.no - Weather data, sunrise/sunset data
* geonames.org - Coordinates -> Timezone
* geoip.ubuntu.com - IP -> Coordinates
* openweathermap.org - Weather data (optional, require API token)
